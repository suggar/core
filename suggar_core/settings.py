"""Contain core initialization logic."""
from suggar_auth.managers import CredentialsManager
from suggar_db.connection import DBConnection
from suggar_db.utils import Singleton

from suggar_core.auth.token_managers import SpotifyTokenManager
from suggar_core.auth.user_managers import DBUserManager
from suggar_core.dao.playlist import PlaylistAccessor
from suggar_core.dao.provider import ProviderAccessor
from suggar_core.dao.spotify import SpotifyAccessor
from suggar_core.dao.token import TokenAccessor
from suggar_core.dao.track import TrackAccessor
from suggar_core.dao.user import UserAccessor
from suggar_core.logic.auth import AuthController
from suggar_core.logic.playlists import PlaylistsController
from suggar_core.logic.tokens import TokenController
from suggar_core.logic.tracks import TracksController
from suggar_core.logic.users import UserController
from suggar_core.spotify.proxy import SpotifyProxy


class Settings(metaclass=Singleton):
    """Provide a singleton with bunch of resources and tools."""

    def __init__(self, config=None):
        # core
        self.config = config
        self.db_connection = DBConnection()
        self.spotify_proxy = SpotifyProxy()

        # data access layer
        self._user_accessor = UserAccessor(self.db_connection)
        self._token_accessor = TokenAccessor(self.db_connection)
        self._provider_accessor = ProviderAccessor(self.db_connection)
        self._tracks_accessor = TrackAccessor(self.db_connection)
        self._playlist_accessor = PlaylistAccessor(self.db_connection)
        self._spotify_accessor = SpotifyAccessor(self.spotify_proxy)

        self.user_controller = UserController(
            self._user_accessor, self._token_accessor,
            self._provider_accessor)
        self.token_controller = TokenController(self._token_accessor)
        self.tracks_controller = TracksController(
            self._tracks_accessor, self._spotify_accessor)
        self.playlist_controller = PlaylistsController(
            self._playlist_accessor, self._spotify_accessor)

        # managers
        self.db_user_manager = DBUserManager(
            self.user_controller, self.token_controller)
        self.spotify_token_manager = SpotifyTokenManager(
            self.spotify_proxy, self.user_controller)

        self.credentials_manager = CredentialsManager(
            self.config.CORE_SERVER_SECRET,
            self.db_user_manager)

        self.auth_controller = AuthController(self.credentials_manager)

    def setup(self, config=None):
        """Perform core components initialization."""
        if config:
            self.config = config
            self.db_connection.db_uri = config.CORE_DB_URI

        # setup database connection
        if not self.config.CORE_IS_TESTING:
            self.db_connection.init_engine()

        # setup spotify proxy
        spotify_creds = (
            self.config.CORE_SPOTIFY_CLIENT_ID,
            self.config.CORE_SPOTIFY_SECRET,
            self.config.CORE_SPOTIFY_REDIRECT_URI)
        self.spotify_proxy.token_manager = self.spotify_token_manager
        self.spotify_proxy.user_manager = self.db_user_manager
        self.spotify_proxy.session_manager = (
            self.credentials_manager.session_manager)
        self.spotify_proxy.setup(spotify_creds)

        # setup credentials manager
        self.credentials_manager.register_token_manager(
            'Spotify', self.spotify_token_manager)
