"""Contains user manager that works with sqlalchemy."""
from suggar_auth.managers import UserManager


class DBUserManager(UserManager):
    """
    Provide database user storage.

    For more info see docstring in UserManager interface
    """

    def __init__(self, user_controller, token_controller):
        self._user_controller = user_controller
        self._token_controller = token_controller

    def search_by_external_id(self, external_id):
        """
        Search for user in database by his external id.

        :param external_id: user's external id
        :return: user object (dict)
        :rtype: dict
        """
        return self._user_controller.search_by_external_id(external_id)

    def update_external_credentials(
            self, external_id, auth_type, payload_list):
        """
        Update user's external credentials.

        :param external_id: user id in external service
        :param auth_type: external service name
        :param payload_list: list of new credentials
        """
        provider_id = self._user_controller.get_provider_id(auth_type)
        return self._token_controller.update_external_credentials(
            external_id, provider_id, payload_list)

    def create_user(self, username, meta=None):
        """
        Create user with provided data.

        :param username:
        :param meta:
        :return:
        """
        return self._user_controller.create_user(username, meta)

    def attach_external_credentials(
            self, internal_id, external_id, auth_type, payload):
        """
        Attach credentials to user.

        :param internal_id:
        :param external_id:
        :param auth_type:
        :param payload:
        :return:
        """
        provider_id = self._user_controller.get_provider_id(auth_type)
        return self._user_controller.attach_external_credentials(
            internal_id, external_id, provider_id, payload)
