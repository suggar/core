"""Contains spotify token manager."""
import datetime

from suggar_auth.exceptions import InvalidCredentialsError
from suggar_auth.managers import TokenManager


class SpotifyTokenManager(TokenManager):
    """Provide spotify token validation logic."""

    def __init__(self, spotify_proxy, users_controller):
        """Initialize spotify token manager with spotify credentials."""
        self.spotify_proxy = spotify_proxy
        self._users_controller = users_controller
        self._provider_id = None

    def get_user_external_token(self, internal_id):
        """
        Retrieve external token for specified user.

        get existing token
        if not token exist:
            return none
        if token expired:
            refresh token
        return token
        """
        provider_id = self.get_provider_id()
        credentials = (
            self._users_controller.get_user_external_credentials(
                internal_id, provider_id))
        if not credentials:
            return None

        external_id, access_token, refresh_token, expiration_date = (
            credentials)

        if datetime.datetime.now() >= expiration_date:
            credentials = self._refresh_token(refresh_token)
            if not credentials:
                return None

            access_token, refresh_token, expires_in = credentials
            self._persist_credentials(
                external_id, access_token,
                refresh_token, expires_in)

        return external_id, access_token, refresh_token, expiration_date

    def get_user_external_creds(self, internal_id):
        """Retrieve user's valid Spotify credentials."""
        return self.get_user_external_token(internal_id)

    def get_user_external_id(self, internal_id):
        """Retrieve user's external id in spotify."""
        provider_id = self.get_provider_id()
        return self._users_controller.get_user_external_credentials(
            internal_id, provider_id)

    def verify_payload(self, payload):
        """
        Retrieve user's id from spotify.

        If not succeeded InvalidCredentialsError is thrown.
        :param payload:  list with access_token, refresh_token and expires_in
        :return: spotify user id
        """
        if not self._check_payload(payload):
            raise InvalidCredentialsError(
                auth_type='Spotify', auth_data=payload)

        code = payload[0]

        credentials = self.spotify_proxy.exchange_code(code)
        access_token, refresh_token, expires_in = credentials
        if not access_token:
            raise InvalidCredentialsError(
                auth_type='Spotify', auth_data=payload)

        external_id = self.spotify_proxy.check_token(access_token)

        payload[0] = access_token
        payload.append(refresh_token)
        payload.append(expires_in)

        self._persist_credentials(
            external_id, access_token, refresh_token, expires_in)

        return external_id

    @staticmethod
    def _check_payload(payload):
        return (
            type(payload) == list and
            len(payload) == 1)

    def get_provider_id(self):
        """Retrieve current provider id."""
        if not self._provider_id:
            self._provider_id = (
                self._users_controller.get_provider_id('Spotify'))

        return self._provider_id

    def _refresh_token(self, refresh_token):
        return self.spotify_proxy.refresh_token(refresh_token)

    def _persist_credentials(
            self, external_id, access_token,
            refresh_token, expires_in):
        """
        Save credentials to database.

        :param internal_id:
        :param external_id:
        :param access_token:
        :param refresh_token:
        :param expires_in:
        :return:
        """
        credentials = (access_token, refresh_token, expires_in)
        self._users_controller.update_external_credentials(
            external_id, 'Spotify', credentials)
