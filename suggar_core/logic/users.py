"""Some logic related to users."""


class UserController:
    """Provide user management logic."""

    def __init__(self, user_accessor, token_accessor, provider_accessor):
        self._user_accessor = user_accessor
        self._token_accessor = token_accessor
        self._provider_accessor = provider_accessor

    def search_by_external_id(self, external_id):
        """
        Search for user in database by his external id.

        :param external_id: user's external id
        :return: user object (dict)
        :rtype: dict
        """
        return self._token_accessor.get_by(external_id)

    def get_user_external_id(self, internal_id, token_provider_id):
        """
        Retrieve user's external id.

        :param internal_id: user's internal id
        :param token_provider_id: service in which user's id is needed
        :return: user's external id in specified service
        """
        credentials = self._token_accessor.get(internal_id, token_provider_id)
        # credentials = users.get_user_external_credentials(
        #     internal_id, token_provider_id)
        if not credentials:
            return None
        return credentials[0]

    def get_user_external_credentials(self, internal_id, token_provider_id):
        """
        Ask model layer for user's external token.

        :param internal_id: user's internal id
        :param token_provider_id: id of token provider
        :return: external_id access_token, refresh_token, expiration_date
        """
        return self._token_accessor.get(internal_id, token_provider_id)

    def get_provider_id(self, provider_name):
        """
        Retrieve provider id by its name.

        :param provider_name: provider unique name
        :return internal numeric id of this provider
        """
        # return users.get_provider_id(provider_name)
        return self._provider_accessor.get_by(provider_name)

    def create_user(self, username, meta=None):
        """
        Create user in database with provided params.

        :param username: user's name
        :param meta: some meta info about user
        :return: dict that contains info about created user
        """
        return self._user_accessor.create(username, meta)

    def attach_external_credentials(
            self, internal_id, external_id, provider_id, payload):
        """
        Attach some external credentials to user.

        :param internal_id: Internal user id
        :param external_id: User id in external service
        :param provider_id: External service's local id
        :param payload: payload containing auth credentials
        """
        self._token_accessor.create(
            internal_id, external_id, provider_id, payload)

    def update_external_credentials(self, external_id, auth_type, payload):
        """
        Update user's existing external credentials.

        :param interal_id: user's internal id
        :param auth_type: method used to authorize user
        :param payload: payload containing auth credentials
        :return: updated token model
        """
        provider_id = self._provider_accessor.get_by(auth_type)
        return self._token_accessor.update(external_id, provider_id, payload)
