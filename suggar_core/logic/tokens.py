"""Contain tools for token management."""


class TokenController:
    """Provide token management logic."""

    def __init__(self, token_accessor):
        self._token_accessor = token_accessor

    def update_external_credentials(self, external_id, provider_id, payload):
        """Update user's credential in some service."""
        return self._token_accessor.update(external_id, provider_id, payload)
