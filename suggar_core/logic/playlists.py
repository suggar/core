"""Logic layer to work with playlists."""


class PlaylistsController:
    """Provide playlist manipulation logic."""

    def __init__(self, playlist_accessor, spotify_accessor):
        self._playlist_accessor = playlist_accessor  # type: CRUDAccessor
        self._spotify_accessor = spotify_accessor

    def create(self, name, description, track_ids, owner_id):
        """Perform playlist creation."""
        return self._playlist_accessor.create(
            name, description, track_ids, owner_id)

    def get(self, playlist_id):
        """Perform playlist retrieval."""
        return self._playlist_accessor.get(playlist_id)

    def delete(self, playlist_id):
        """Perform playlist deletion."""
        return self._playlist_accessor.delete(playlist_id)

    def update(self, playlist_id, name=None, description=None, track_ids=None):
        """Perform playlist update."""
        return self._playlist_accessor.update(
            playlist_id, name, description, track_ids)

    def push_playlist(self, playlist):
        """Push playlist into Spotify."""
        return self._spotify_accessor.push_playlist(playlist)

    def get_users_playlists(self, internal_id):
        """Get user's own playlists."""
        return self._playlist_accessor.get_by(internal_id)
