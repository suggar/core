"""Utils for tracks processing."""


class TracksController:
    """Provide tracks manipulation logic."""

    def __init__(self, tracks_accessor, spotify_accessor):
        self._tracks_accessor = tracks_accessor
        self._spotify_accessor = spotify_accessor

    def search_tracks(self, search_query, **kwargs):
        """Search for track by a search string."""
        search_result = self._spotify_accessor.search_tracks(
            query=search_query, **kwargs)

        try:
            search_items = search_result['tracks']['items']
        except KeyError:
            return None

        track_list = [{
            'track_id': track.get('id'),
            'title': track.get('name'),
            'artist':
                ', '.join([artist['name'] for artist in track.get('artists')])
                if track.get('artists') else
                'Unknown Artist',
            'preview_url': track['preview_url']
        } for track in search_items]

        self.create_many(track_list)

        return track_list

    def create_track(self, id, title, artist, meta=None, preview_url=None):
        """Create track based on provided data."""
        return self._tracks_accessor.create(
            id=id,
            title=title,
            artist=artist,
            meta=meta,
            preview_url=preview_url)

    def create_many(self, tracks):
        """Create bunch of tracks."""
        self._tracks_accessor.create_many(tracks)

    def update(self, track_id, title=None, artist=None, preview_url=None,
               meta=None):
        """Update track identified by track_id with provided data."""
        return self._tracks_accessor.update(
            track_id=track_id,
            title=title,
            artist=artist,
            preview_url=preview_url,
            meta=meta
        )

    def update_many(self, tracks):
        """Update bunch of tracks in a single transaction."""
        return self._tracks_accessor.update_many(tracks)

    def get_track(self, track_id):
        """
        Retrieve track by its id from.

        :param track_id: The track id itself
        :return: dict with track parameters
        """
        return self._tracks_accessor.get(track_id)

    def get_tracks(self, offset=None, count=None, start_date=None,
                   end_date=None):
        """Retrieve track list."""
        return self._tracks_accessor.get_many(
            offset, count, start_date=start_date, end_date=end_date)
