"""Contain tools for user authorization."""


class AuthController:
    """Provide user authorization logic."""

    def __init__(self, credentials_manager):
        self._credentials_manager = credentials_manager

    def obtain_local_token(self, auth_type, auth_data):
        """Obtain token to authorize in this service."""
        return self._credentials_manager.obtain_local_token(
            auth_type, auth_data)
