"""Contains stuff to access provider instances."""
from suggar_core.dao.common import CRUDAccessor
from suggar_core.models.users import TokenProvider


class ProviderAccessor(CRUDAccessor):
    """Provide CRUD access to provider objects."""

    def __init__(self, db_connection):
        self._db_connection = db_connection

    def get_by(self, provider_name):
        """Return provider id for specified provider name."""
        connection = self._db_connection
        with connection.create_session() as session:
            query = session.query(TokenProvider)
            filtered = query.filter(TokenProvider.name == provider_name)
            provider_model = filtered.first()  # type: TokenProvider

            if not provider_model:
                return None

            return provider_model.id
