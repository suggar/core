"""Contains stuff to access spotify."""


class SpotifyAccessor:
    """Provide custom access to spotify data."""

    def __init__(self, spotify_proxy):
        self._spotify_proxy = spotify_proxy

    def search_tracks(self, query, **kwargs):
        """Send search query to spotify."""
        return self._spotify_proxy.search_tracks(q=query, **kwargs)

    def push_playlist(self, playlist_dict):
        """Push specified playlist into Spotify."""
        external_playlist = self._spotify_proxy.playlist_create(
            playlist_dict.get('name'),
            playlist_dict.get('description'))
        external_playlist_id = external_playlist.get('id')

        tracks = playlist_dict.get('tracks')
        track_ids = [track['track_id'] for track in tracks]

        self._spotify_proxy.playlist_replace_tracks(
            external_playlist_id, track_ids)

        return external_playlist
