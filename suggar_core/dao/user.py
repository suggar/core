"""Contains stuff to access token instances."""
import datetime

from suggar_core.dao.common import CRUDAccessor
from suggar_core.models.users import User


class UserAccessor(CRUDAccessor):
    """Provide CRUD access to user objects."""

    def __init__(self, db_connection):
        self._db_connection = db_connection

    def get(self, internal_id):
        """
        Find user by his internal id.

        :param internal_id:
        :return: user dict
        """
        connection = self._db_connection
        with connection.create_session() as session:
            query = session.query(User)
            query = query.filter(User.id == internal_id)
            user = query.first()

            if not user:
                return None
            return user.as_dict()

    def create(self, username, meta=None):
        """
        Create new user in database with provided info.

        :param username: unique user's name
        :param meta: some meta info to store in db
        :return: user object (dict)
        :rtype dict
        """
        user = User(username=username, created_at=datetime.datetime.now())
        if meta:
            user.meta = meta

        connection = self._db_connection
        with connection.create_session() as session:
            session.add(user)
            session.flush()

            user_dict = user.as_dict()

        return user_dict

    def update(self, internal_id, username=None, meta=None):
        """
        Update user identified by internal id with provided data.

        :param internal_id: user's internal id
        :param username: new username
        :param meta: new meta data
        :return: updated user dict
        """
        user = self.get(internal_id)
        if not user:
            return None

        if username:
            user.username = username

        if meta:
            user.meta = meta

        connection = self._db_connection
        with connection.create_session() as session:
            session.add(user)
            session.flush()

            user_dict = user.as_dict()

        return user_dict
