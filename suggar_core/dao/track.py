"""Contains stuff to access track instances."""
from sqlalchemy.orm.attributes import flag_modified

from suggar_core.dao.common import CRUDAccessor
from suggar_core.models.tracks import Track


class TrackAccessor(CRUDAccessor):
    """Provide CRUD access to track objects."""

    def __init__(self, db_connection):
        self._db_connection = db_connection

    def get(self, track_id):
        """Retrieve track from database by its id."""
        connection = self._db_connection
        with connection.create_session() as session:
            query = session.query(Track)
            filtered = query.filter(Track.track_id == track_id)
            track_model = filtered.first()

            if not track_model:
                return None

            track_dict = track_model.as_dict()

        return track_dict

    def create(self, id, title, artist, meta=None, preview_url=None):
        """Create track with provided data."""
        connection = self._db_connection
        with connection.create_session() as session:
            track = Track(
                track_id=id,
                title=title,
                artist=artist,
                preview_url=preview_url,
                meta=meta)
            session.merge(track)
            session.flush()
            track_dict = track.as_dict()

        return track_dict

    def create_many(self, tracks):
        """
        Create list of tracks.

        :param tracks list of track dicts. see create for dict keys
        """
        connection = self._db_connection
        with connection.create_session() as session:
            for track in tracks:
                track_model = Track(
                    track_id=track.get('track_id'),
                    title=track.get('title'),
                    artist=track.get('artist'),
                    preview_url=track.get('preview_url'),
                    meta=track.get('meta'))
                session.merge(track_model)

    def update(self, track_id, title=None, artist=None, preview_url=None,
               meta=None):
        """Update track identified by it's id with provided data."""
        connection = self._db_connection
        with connection.create_session() as session:
            query = session.query(Track)
            query = query.filter(Track.track_id == track_id)
            track = query.first()
            if not track:
                return None

            if title is not None:
                track.title = title
            if artist is not None:
                track.artist = artist
            if preview_url is not None:
                track.preview_url = preview_url

            if meta is not None:
                if not track.meta:
                    track.meta = {}
                for field_name, field_value in meta.items():
                    track.meta[field_name] = field_value

            session.add(track)

    def update_many(self, tracks):
        """Update bunch of tracks in a single session."""
        track_ids = list(tracks.keys())
        connection = self._db_connection
        with connection.create_session() as session:
            query = session.query(Track)
            query = query.filter(Track.track_id.in_(track_ids))
            track_models = query.all()
            for track_model in track_models:
                current_title = (
                    tracks.get(track_model.track_id).get('title'))
                current_artist = (
                    tracks.get(track_model.track_id).get('artist'))
                current_preview_url = (
                    tracks.get(track_model.track_id).get('preview_url'))
                current_meta = (
                    tracks.get(track_model.track_id).get('meta'))

                if current_title:
                    track_model.title = current_title
                if current_artist:
                    track_model.artist = current_artist
                if current_preview_url:
                    track_model.preview_url = current_preview_url

                if current_meta is not None:
                    if not track_model.meta:
                        track_model.meta = {}
                    for field_name, field_value in current_meta.items():
                        track_model.meta[field_name] = field_value
                    flag_modified(track_model, 'meta')
                session.merge(track_model)
            session.flush()

    def get_many(self, offset=None, count=None,
                 start_date=None, end_date=None):
        """Retrieve track from db."""
        connection = self._db_connection
        with connection.create_session() as session:
            query = session.query(Track)
            if start_date:
                start_date_str = start_date.strftime('%Y-%m-%d')
                query = query.filter(
                    Track.meta['release_date'] >= start_date_str)
                if end_date:
                    end_date_str = end_date.strftime('%Y-%m-%d')
                    query = query.filter(
                        Track.meta['release_date'] <= end_date_str)

            if offset:
                query = query.offset(offset)
            if count:
                query = query.limit(count)

            track_models = query.all()

            return [track_model.as_dict() for track_model in track_models]
