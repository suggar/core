"""Contains stuff to access token instances."""
import datetime

from suggar_core.dao.common import CRUDAccessor
from suggar_core.models.users import UserToken


class TokenAccessor(CRUDAccessor):
    """Provide CRUD access to token objects."""

    def __init__(self, db_connection):
        self._db_connection = db_connection

    def get(self, internal_id, token_provider_id):
        """
        Search database for user's external credentials.

        :param internal_id: user's internal id
        :param token_provider_id: id of token provider
        :return: external_id access_token, refresh_token, expiration_date
        """
        connection = self._db_connection
        with connection.create_session() as session:
            query = session.query(UserToken)
            filtered = query.filter(UserToken.user_id == internal_id,
                                    UserToken.provider_id == token_provider_id)
            token_model = filtered.first()  # type: UserToken

            if not token_model:
                return None

            return (
                token_model.external_id,
                token_model.access_token,
                token_model.refresh_token,
                token_model.expires_at)

    def get_by(self, external_id):
        """
        Search in database for user with attached external_id.

        :param external_id: external id to search for
        :return: user object (dict)
        :rtype dict
        """
        connection = self._db_connection
        with connection.create_session() as session:
            query = session.query(UserToken)
            filtered = query.filter(UserToken.external_id == external_id)
            token_model = filtered.first()  # type: UserToken

            if not token_model:
                return None

            user_dict = token_model.user.as_dict()
        return user_dict

    def create(self, internal_id, external_id, provider_id, payload):
        """
        Attach some external credentials to user.

        :param internal_id: user's internal id
        :param external_id: user's id in external service
        :param provider_id: id of service that provided credentials
        :param payload: list with credentials in it
        :return token info dict
        """
        DEFAULT_EXPIRES_IN = 3600

        access_token = payload[0]
        refresh_token = payload[1]

        expires_in = payload[2]

        try:
            expires_in = int(expires_in)
        except ValueError:
            expires_in = DEFAULT_EXPIRES_IN
        expiration_date = TokenAccessor._get_token_expiration_date(expires_in)

        connection = self._db_connection
        with connection.create_session() as session:
            token_model = UserToken(
                user_id=internal_id,
                provider_id=provider_id,
                external_id=external_id,
                access_token=access_token,
                refresh_token=refresh_token,
                expires_at=expiration_date)
            session.add(token_model)
            return token_model.as_dict()

    def update(self, external_id, provider_id, payload):
        """
        Update existing credentials in db.

        :param external_id: user's id in external service
        :param provider_id: id of service that provided credentials
        :param payload: list with credentials in it
        :return: updated token object (dict)
        """
        if not payload:
            return None
        access_token, refresh_token, expires_in = payload

        connection = self._db_connection
        with connection.create_session() as session:
            query = session.query(UserToken)
            filtered = query.filter(UserToken.external_id == external_id,
                                    UserToken.provider_id == provider_id)
            token_model = filtered.first()  # type: UserToken

            if not token_model:
                return None

            if access_token:
                token_model.access_token = access_token
            if refresh_token:
                token_model.refresh_token = refresh_token
            if expires_in:
                token_model.expires_at = (
                    TokenAccessor._get_token_expiration_date(expires_in))

            return token_model.as_dict()

    @staticmethod
    def _get_token_expiration_date(expires_in):
        """
        Calculate expiration date based on expires_in param.

        :param expires_in: seconds from current moment
        :return: datetime when the token won't be fresh anymore
        """
        SAFE_DELTA = 60
        time_delta = datetime.timedelta(seconds=int(expires_in) - SAFE_DELTA)
        return datetime.datetime.now() + time_delta
