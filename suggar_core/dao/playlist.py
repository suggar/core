"""Contains stuff to access playlist instances."""
from suggar_core.dao.common import CRUDAccessor
from suggar_core.models.playlists import PlaylistModel
from suggar_core.models.tracks import TrackInPlaylistModel


class PlaylistAccessor(CRUDAccessor):
    """Provide CRUD access to playlist objects."""

    def __init__(self, db_connection):
        self._db_connection = db_connection

    def get(self, playlist_id):
        """Retrieve a single playlist with track ids."""
        connection = self._db_connection
        with connection.create_session() as session:
            query = session.query(PlaylistModel)
            query = query.filter(PlaylistModel.id == playlist_id)
            playlist = query.first()
            try:
                playlist = playlist.as_dict()
            except AttributeError:
                return None

        return playlist

    def get_by(self, user_internal_id):
        """Retrieve user's playlists."""
        connection = self._db_connection
        with connection.create_session() as session:
            query = session.query(PlaylistModel)
            query = query.filter(PlaylistModel.owner_id == user_internal_id)
            playlists = query.all()

            return [playlist.as_dict() for playlist in playlists]

    def create(self, name, description, track_ids, owner_id):
        """Create playlist with specified parameters."""
        playlist_model = PlaylistModel(
            name=name,
            description=description,
            owner_id=owner_id)

        connection = self._db_connection
        with connection.create_session() as session:
            session.add(playlist_model)
            session.flush()

            session.add(playlist_model)

            session.add_all([
                TrackInPlaylistModel(
                    track_id=track_id,
                    playlist_id=playlist_model.id)
                for track_id in track_ids])

            playlist_dict = playlist_model.as_dict()

        return playlist_dict

    def delete(self, playlist_id):
        """Remove playlist with specified id."""
        connection = self._db_connection
        with connection.create_session() as session:
            query = session.query(PlaylistModel)
            query = query.filter(PlaylistModel.id == playlist_id)
            playlist_model = query.first()

            playlist_dict = playlist_model.as_dict()
            session.delete(playlist_model)

        return playlist_dict

    def update(
            self, playlist_id, name=None, description=None, track_ids=None):
        """Update specified playlist with full or partial information."""
        connection = self._db_connection
        with connection.create_session() as session:
            query = session.query(PlaylistModel)
            query = query.filter(PlaylistModel.id == playlist_id)
            playlist_model = query.first()

            if name:
                playlist_model.name = name

            if description:
                playlist_model.description = description

            if track_ids is not None:
                query = session.query(TrackInPlaylistModel)
                query = query.filter(
                    TrackInPlaylistModel.playlist_id == playlist_id)
                query.delete()

                for track_id in track_ids:
                    session.add(TrackInPlaylistModel(
                        playlist_id=playlist_id, track_id=track_id))

            # session.add(playlist_model)
            session.flush()
            playlist_dict = playlist_model.as_dict()

        return playlist_dict
