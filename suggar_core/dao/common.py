"""Contains an interface for CRUD access to object."""


class CRUDAccessor:
    """Provide methods for CRUDing on objects."""

    def get(self, *args, **kwargs):
        """
        Retrieve single instance by it's unique key.

        :return: single instance
        """
        raise NotImplementedError('Interface method call')

    def get_many(self, offset=None, count=None, *args, **kwargs):
        """
        Retrieve bunch of instances.

        offset and count are for pagination.
        :return: list of instances
        """
        raise NotImplementedError('Interface method call')

    def get_by(self, *args, **kwargs):
        """
        Retrieve instance by some other unique attribute.

        :return: single instance
        """
        raise NotImplementedError('Interface method call')

    def create(self, *args, **kwargs):
        """
        Create instance with provided arguments.

        :return: created instance
        """
        raise NotImplementedError('Interface method call')

    def update(self, *args, **kwargs):
        """
        Update instance identified by a key with provided data.

        :return: updated instance
        """
        raise NotImplementedError('Interface method call')

    def delete(self, *args, **kwargs):
        """
        Delete instance identified by a key.

        :return: deleted instance
        """
        raise NotImplementedError('Interface method call')
