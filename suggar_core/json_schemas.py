"""Schemas to validate JSON requests."""

create_playlist_schema = {
    'type': 'object',
    'properties': {
        'name': {'type': 'string'},
        'description': {'type': 'string'},
        'tracks': {
            'type': 'array',
            'items': {
                'type': 'string'
            }
        }
    },
    'required': ['name', 'description', 'tracks']
}

update_playlist_schema = {
    'type': 'object',
    'properties': {
        'name': {'type': 'string'},
        'description': {'type': 'string'},
        'tracks': {
            'type': 'array',
            'items': {
                'type': 'string'
            }
        }
    }
}
