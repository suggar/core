"""Contain default config class to inherit from."""


class CoreConfigDefault:
    """Basic configuration to inherit from."""

    CORE_DB_URI = ''
    CORE_IS_TESTING = False

    CORE_SERVER_SECRET = ''

    CORE_SPOTIFY_CLIENT_ID = ''
    CORE_SPOTIFY_SECRET = ''
    CORE_SPOTIFY_REDIRECT_URI = ''
