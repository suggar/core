"""Contain bunch of unittests for playlist retrieval process."""
from unittest.mock import MagicMock

from suggar_core.dao.playlist import PlaylistAccessor


def test_get_playlist_right_query():
    """Check if db request is correct."""
    playlist_id = 'some_test_id'

    # Prepare mocks
    connection_mock = MagicMock()
    session_mock = MagicMock()
    connection_mock.create_session.return_value = session_mock

    playlist_accessor = PlaylistAccessor(connection_mock)

    # act
    playlist_accessor.get(playlist_id)

    # check whether this stuff worked well
    filter_call = session_mock.__enter__().query().filter

    filter_call.assert_called_once()

    filter_params = filter_call.call_args[0][0]
    assert filter_params.right.value == playlist_id


def test_get_playlist_not_exists():
    """Check if None is returned when no playlists found."""
    # Prepare mocks
    connection_mock = MagicMock()
    session_mock = MagicMock()
    connection_mock.create_session.return_value = session_mock

    session_mock.__enter__().query().filter().first.return_value = None

    player_accessor = PlaylistAccessor(connection_mock)

    assert player_accessor.get('does not actually matters') is None


def test_get_playlist_exists():
    """Check if valid object is returned when id is correct."""
    expected_object = 'foo_bar'

    # Prepare mocks
    connection_mock = MagicMock()
    session_mock = MagicMock()
    connection_mock.create_session.return_value = session_mock

    session_mock.__enter__().query().filter().first().as_dict.return_value = (
        expected_object)

    playlist_accessor = PlaylistAccessor(connection_mock)

    returned_object = playlist_accessor.get('some_id')

    assert expected_object == returned_object
