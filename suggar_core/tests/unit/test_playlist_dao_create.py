"""Contain bunch of unittests for playlist creation process."""
from unittest.mock import call
from unittest.mock import MagicMock
from unittest.mock import patch

import jsonschema

from suggar_core import json_schemas
from suggar_core.dao.playlist import PlaylistAccessor


def test_create_playlist_right_name_and_description():
    """Check if the name and description in query are valid."""
    name = 'test_name_rab4ntjhwb5hj'
    owner_id = 13
    description = 'test_description_aswibty43hwwykj'

    connection_mock = MagicMock()
    session_mock = MagicMock()
    connection_mock.create_session.return_value = session_mock

    playlist_accessor = PlaylistAccessor(connection_mock)

    playlist_accessor.create(
        name=name,
        description=description,
        owner_id=owner_id,
        track_ids=[])

    added_playlist = session_mock.__enter__().add.call_args[0][0]

    session_mock.__enter__().add.assert_called()
    assert added_playlist.name == name
    assert added_playlist.description == description


@patch('suggar_core.dao.playlist.PlaylistModel')
def test_create_playlist_right_track_linkage(playlist_model_mock):
    """Check if all tracks are in the session with correct playlist."""
    playlist_id = 523452
    owner_id = 13
    name = 'test_name_rab4ntjhwb5hj'
    description = 'test_description_aswibty43hwwykj'
    track_ids = [
        'id1_fa4eygw5yw45',
        'id2_af3g5ruhj4ef',
        'id3_raf2qy33w5u5']

    # Prepare mocks
    connection_mock = MagicMock()
    session_mock = MagicMock()
    connection_mock.create_session.return_value = session_mock

    playlist_mock = playlist_model_mock.return_value
    playlist_mock.id = playlist_id

    playlist_accessor = PlaylistAccessor(connection_mock)

    # Execute testable func
    playlist_accessor.create(
        name=name,
        description=description,
        owner_id=owner_id,
        track_ids=track_ids)

    # Check mocks
    session_mock.assert_has_calls([
        call.__enter__().add(playlist_mock),
        call.__enter__().flush(),
        call.__enter__().add(playlist_mock),
    ])

    added_tracks = session_mock.__enter__().add_all.call_args[0][0]
    assert len(added_tracks) == len(track_ids)
    for added_track in added_tracks:
        assert added_track.track_id in track_ids
        assert added_track.playlist_id == playlist_id


def test_create_playlist_returns_valid_dict():
    """Check if the dict returned is valid."""
    name = 'test_name_rab4ntjhwb5hj'
    description = 'test_description_aswibty43hwwykj'
    owner_id = 13

    # Prepare mocks
    connection_mock = MagicMock()
    session_mock = MagicMock()
    connection_mock.create_session.return_value = session_mock

    playlist_accessor = PlaylistAccessor(connection_mock)

    returned_playlist = playlist_accessor.create(
        name=name,
        description=description,
        owner_id=owner_id,
        track_ids=[])

    jsonschema.validate(
        returned_playlist,
        json_schemas.create_playlist_schema)

    assert returned_playlist['name'] == name
    assert returned_playlist['description'] == description
