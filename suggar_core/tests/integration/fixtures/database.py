"""Contain test database fixture."""
import os

from pytest import fixture
from suggar_db.connection import DBConnection

from suggar_core.models.base import BaseModel
from suggar_core.models.playlists import PlaylistModel  # noqa
from suggar_core.models.tracks import Track


def _get_db_uri():
    # base_dir = os.path.dirname(__file__)
    # db_name = os.environ.get('DB_NAME')
    # db_filename = os.path.join(base_dir, db_name + '.sqlite3')

    return os.environ.get('DB_URI')


def _fill_test_data(connection: DBConnection):
    tracks = [
        {
            'track_id': 'g3r4hw45yh4wert',
            'title': 'Some title',
            'artist': 'Various artist',
            'preview_url': None,
            'meta': {}},
        {
            'track_id': 'gq53eryhw45yhuw4',
            'title': 'Another title',
            'artist': 'Unknown artist',
            'preview_url': None,
            'meta': {}},
        {
            'track_id': 'j567ii5re6tyje5r',
            'title': 'Soviet March',
            'artist': 'Red Alert 3',
            'preview_url': None,
            'meta': {}}
    ]

    with connection.create_session() as session:
        for track in tracks:
            track_model = Track(
                track_id=track.get('track_id'),
                title=track.get('title'),
                artist=track.get('artist'),
                preview_url=track.get('preview_url'),
                meta=track.get('meta'))
            session.merge(track_model)


@fixture(scope='session')
def db_connection():
    """Provide test database connection."""
    db_uri = _get_db_uri()

    db_conn = DBConnection(db_uri)
    db_conn.init_engine()

    BaseModel.metadata.create_all(db_conn.db_engine)
    _fill_test_data(db_conn)

    return db_conn
