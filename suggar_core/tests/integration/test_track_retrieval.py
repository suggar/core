"""Contain tests of track retrieval process."""
from unittest.mock import MagicMock

from suggar_core.dao.track import TrackAccessor
from suggar_core.logic.tracks import TracksController
from suggar_core.tests.integration.fixtures.database import db_connection  # noqa


def test_track_retrieval_known(db_connection):  # noqa
    """Check whether get_track returns valid not null response."""
    spotify_accessor = MagicMock()
    tracks_controller = TracksController(
        TrackAccessor(db_connection), spotify_accessor)

    valid_track_id = 'g3r4hw45yh4wert'

    track = tracks_controller.get_track(valid_track_id)

    assert track is not None


def test_track_retrieval_unknown(db_connection):  # noqa
    """Check whether get_track returns None if track not found."""
    spotify_accessor = MagicMock()
    tracks_controller = TracksController(
        TrackAccessor(db_connection), spotify_accessor)

    invalid_track_id = 'gaew4w34g3qerg'

    track = tracks_controller.get_track(invalid_track_id)

    assert track is None
