"""Contain BaseModel to inherit from."""
from sqlalchemy.ext.declarative import declarative_base

BaseModel = declarative_base()
