"""Contains models that stores some user info."""
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relation

from suggar_core.models.base import BaseModel

# settings = Settings()
# connection = settings.db_connection
# BaseModel = connection.BaseModel


class User(BaseModel):
    """User model."""

    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, nullable=False)
    username = Column(String(255), unique=True, nullable=False)
    password_hash = Column(String(255), nullable=True)

    created_at = Column(DateTime, nullable=False)
    meta = Column(JSONB, nullable=True)

    def as_dict(self):
        """Return plain dict of user."""
        return {
            'id': self.id,
            'username': self.username,
            'password_hash': self.password_hash,
            'created_at': self.created_at,
            'meta': self.meta or {}}


class TokenProvider(BaseModel):
    """Token provider model."""

    __tablename__ = 'token_provider'
    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String(255), unique=True, nullable=False)

    def as_dict(self):
        """Return plain dict of token provider."""
        return {
            'id': self.id,
            'name': self.name}


class UserToken(BaseModel):
    """An external token set that was issued by provider for user."""

    __tablename__ = 'user_token'

    user_id = Column(
        Integer, ForeignKey('user.id'),
        primary_key=True, nullable=False)
    provider_id = Column(
        Integer, ForeignKey('token_provider.id'),
        primary_key=True, nullable=False)

    user = relation('User')
    provider = relation('TokenProvider')

    external_id = Column(String(255), nullable=False)
    access_token = Column(String(255), nullable=False)
    refresh_token = Column(String(255), nullable=True)
    expires_at = Column(DateTime, nullable=True)

    def as_dict(self):
        """Return plain dict of user token."""
        return {
            'internal_id': self.user_id,
            'provider_id': self.provider_id,
            'external_id': self.external_id,
            'access_token': self.access_token,
            'refresh_token': self.refresh_token,
            'expires_at': self.expires_at}
