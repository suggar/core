"""Data manipulation related to playlists."""
from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy.orm import relationship

from suggar_core.models.base import BaseModel
from suggar_core.models.users import User


class PlaylistModel(BaseModel):
    """Playlist data model."""

    __tablename__ = 'playlist'

    id = Column(Integer, primary_key=True, nullable=False)

    name = Column(String(200), nullable=False)
    description = Column(Text, nullable=False)

    owner_id = Column(Integer, ForeignKey('user.id'))
    owner = relationship(User)

    tracks = relationship(
        'TrackInPlaylistModel',
        back_populates='playlist',
        lazy='joined',
        cascade='all, merge, delete, delete-orphan')

    def as_dict(self):
        """Return plain dict based on object's data."""
        tracks = []
        for track in self.tracks:
            if track.track:
                tracks.append(track.track.as_dict())
            else:
                tracks.append(track.as_dict())

        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'owner_id': self.owner_id,
            'tracks': tracks}

    def __repr__(self):
        """Return short string representation of a playlist."""
        return '<Playlist "{}">'.format(self.name)
