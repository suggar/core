"""Data manipulation related to tracks."""
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship

from suggar_core.models.base import BaseModel

# settings = Settings()
# connection = settings.db_connection
# BaseModel = connection.BaseModel


class TrackInPlaylistModel(BaseModel):
    """Linking model between spotify track id and playlist."""

    __tablename__ = 'track_in_playlist'

    playlist_id = Column(
        Integer,
        ForeignKey('playlist.id'),
        primary_key=True,
        nullable=False)
    track_id = Column(
        String(255),
        ForeignKey('track.track_id'),
        primary_key=True,
        nullable=False)

    playlist = relationship(
        'PlaylistModel', back_populates='tracks', lazy='joined')
    track = relationship(
        'Track', back_populates='playlists', lazy='joined')

    def as_dict(self):
        """Return plain dict from this link model."""
        return {
            'playlist_id': self.playlist_id,
            'track_id': self.track_id}

    def __repr__(self):
        """Return short string representation of a track linkage."""
        return '<Track {} in Playlist {}>'.format(
            self.track_id, str(self.playlist_id))


class Track(BaseModel):
    """Track model."""

    __tablename__ = 'track'

    track_id = Column(String(255), primary_key=True, nullable=False)

    title = Column(String(255), nullable=False)
    artist = Column(String(255), nullable=False)
    preview_url = Column(String(255), nullable=True)
    meta = Column(JSONB, nullable=True)

    playlists = relationship(
        'TrackInPlaylistModel',
        back_populates='track',
        lazy='joined')

    def as_dict(self):
        """Return plain dict from this track model."""
        return {
            'track_id': self.track_id,
            'title': self.title,
            'artist': self.artist,
            'meta': self.meta or {},
            'preview_url': self.preview_url or None}

    def __repr__(self):
        """Return short string representation of a track."""
        return '<Track {}>'.format(self.track_id)
