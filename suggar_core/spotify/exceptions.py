"""Contain exceptions that can appear during spotify proxy work."""


class SpotifyError(Exception):
    """
    Raised if any spotify error happens.

    This is base error for every error in auth package.
    """

    message = ''

    def __init__(self, message='Unable to authenticate'):
        Exception.__init__(self, message)
        self.message = message

    def __str__(self):
        """Make string representation of exception."""
        return self.message
