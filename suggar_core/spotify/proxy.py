"""This module contains methods to work with spotify."""
from spotipy import Spotify, SpotifyException
from spotipy import SpotifyException as SpotipyException
from spotipy.oauth2 import SpotifyClientCredentials, SpotifyOauthError
from spotipy.oauth2 import SpotifyOAuth

from suggar_core.spotify.exceptions import SpotifyError


class SpotifyProxy:
    """Proxy class to make token stuff transparent."""

    def __init__(
            self, spotify_token_manager=None, user_manager=None,
            session_manager=None, spotify_creds=None):
        """Initialize instance with spotify credentials manager."""
        self.token_manager = spotify_token_manager
        self.session_manager = session_manager
        self.user_manager = user_manager
        self._spotify_creds = spotify_creds
        self._client_spotify = None

    def setup(self, spotify_creds):
        """Perform actual initialization of components."""
        self._spotify_creds = spotify_creds
        client_id, client_secret, redirect_uri = spotify_creds
        client_creds_manager = SpotifyClientCredentials(
            client_id=client_id,
            client_secret=client_secret)
        self._client_spotify = Spotify(
            client_credentials_manager=client_creds_manager)

    def configure(self, spotify_token_manager=None, user_manager=None):
        """Reconfigure instance with provided managers."""
        self.token_manager = spotify_token_manager
        self.user_manager = user_manager

    def refresh_token(self, refresh_token):
        """
        Given refresh token returns corresponding fresh access token.

        :param refresh_token:
        :return:
        """
        oauth = self._get_oauth_instance()
        try:
            new_credentials = oauth.refresh_access_token(refresh_token)
        except SpotipyException:
            return None, None, None

        access_token = new_credentials['access_token']
        refresh_token = new_credentials['refresh_token']
        expires_in = new_credentials['expires_in']

        return access_token, refresh_token, expires_in

    def search_tracks(self, *args, **kwargs):
        """Proxy spotify search call."""
        return self._client_spotify.search(*args, **kwargs)

    def track(self, *args, **kwargs):
        """Proxy spotify track retrieval."""
        return self._client_spotify.track(*args, **kwargs)

    def get_new_albums(self, *args, **kwargs):
        """Proxy spotify new albums retrieval."""
        return self._client_spotify.new_releases(*args, **kwargs)

    def get_albums(self, albums_ids):
        """Proxy spotify albums retrieval."""
        return self._client_spotify.albums(albums_ids)

    def get_tracks(self, tracks_ids, market=None):
        """Proxy spotify track retrieval."""
        return self._client_spotify.tracks(tracks_ids, market)

    def get_track_feature(self, tracks_ids):
        """Proxy spotify track feature call."""
        return self._client_spotify.audio_features(tracks=tracks_ids)

    def me(self):
        """Proxy spotify me call."""
        internal_id = self.session_manager.load_user()
        credentials = self.token_manager.get_user_external_token(internal_id)
        spotify_instance = Spotify(auth=credentials[1])

        try:
            info = spotify_instance.me()
        except SpotipyException as error:
            return error.msg

        return info

    def check_token(self, token):
        """
        Check if the token is valid spotify token.

        :return token owner's id (external, spotify)
        """
        spotify_instance = Spotify(auth=token)
        try:
            spotify_user_info = spotify_instance.me()
        except SpotipyException:
            return None

        return spotify_user_info.get('id')

    def get_new_tracks(self):
        """Retrieve just-released tracks from spotify."""
        spotify_albums = self.get_new_albums()

        try:
            spotify_albums = spotify_albums['albums']['items']
        except KeyError:
            raise SpotifyError('Empty or invalid response (get_new_albums)')

        albums_ids = [
            spotify_album['id']
            for spotify_album in spotify_albums]

        albums_with_tracks = self.get_albums(albums_ids)

        try:
            albums_with_tracks = albums_with_tracks['albums']
        except KeyError:
            raise Exception('Empty or invalid response (get_albums)')

        tracks = []
        for album in albums_with_tracks:
            for track in album['tracks']['items']:
                track['album'] = album['name']
                track['release_date'] = album['release_date']
                tracks.append(track)

        return tracks

    def playlist_create(self, name, description='', is_public=False):
        """Proxy spotify search call."""
        external_id, token = self._get_current_external_credentials()

        spotify = Spotify(auth=token)
        return spotify.user_playlist_create(
            external_id,
            name,
            is_public)

    def playlist_replace_tracks(self, playlist_id, tracks):
        """Proxy spotify search call."""
        external_id, token = self._get_current_external_credentials()

        spotify = Spotify(auth=token)

        try:
            result = spotify.user_playlist_replace_tracks(
                external_id,
                playlist_id,
                tracks)
        except SpotifyException:
            raise SpotifyError('Invalid tracks')
        return result

    def exchange_code(self, code):
        """
        Exchange auth code for credentials.

        :param code:
        :return: tuple
        """
        oauth = self._get_oauth_instance()

        try:
            credentials = oauth.get_access_token(code)
        except SpotifyOauthError:
            return None, None, None

        return (
            credentials['access_token'],
            credentials['refresh_token'],
            credentials['expires_in'])

    def _get_oauth_instance(self):
        if not self._spotify_creds:
            raise SpotifyError(
                "Can't construct oauth instance without credentials")

        client_id, client_secret, redirect_uri = self._spotify_creds

        return SpotifyOAuth(
            client_id=client_id, client_secret=client_secret,
            redirect_uri=redirect_uri)

    def _get_current_external_credentials(self):
        current_internal_id = self.session_manager.load_user()
        external_id, token, _, _ = self.token_manager.get_user_external_token(
            current_internal_id)

        return external_id, token
