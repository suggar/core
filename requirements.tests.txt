flake8==3.5.0
flake8-docstrings==1.3.0
flake8-import-order==0.17.1
flake8-polyfill==1.0.2
flake8-quotes==0.14.0
pytest==3.5.0
pytest-cov==2.5.1
jsonschema==2.6.0