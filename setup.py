"""Settings for proper package installation."""
from setuptools import setup


setup(
    name='suggar_core',
    version='0.1.3',
    description='Core mechanisms for Suggar',
    url='https://gitlab.com/suggar_core/core',
    author='Nef1k',
    author_email='nef1k@outlook.com',
    license='MIT',
    packages=[
        'suggar_core',
        'suggar_core.auth',
        'suggar_core.dao',
        'suggar_core.logic',
        'suggar_core.models',
        'suggar_core.spotify'],
    install_requires=[
        'sqlalchemy==1.2.8',
        'boto3==1.7.19',
        'spotipy==2.4.4'],
    dependency_links=[
        'git+https://gitlab.com/suggar/auth.git',
        'git+https://gitlab.com/suggar/database.git'
    ],
    zip_safe=False)
