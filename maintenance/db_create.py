import os

from suggar_db.connection import DBConnection

from suggar_core.models.base import *
from suggar_core.models.playlists import *
from suggar_core.models.tracks import *
from suggar_core.models.users import *


if __name__ == '__main__':
    db_uri = os.environ.get("DB_URI")
    connection = DBConnection(db_uri=db_uri)
    connection.init_engine()

    BaseModel.metadata.create_all(connection.db_engine)
